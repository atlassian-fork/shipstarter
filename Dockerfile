FROM node:6
ENV AC_LOCAL_BASE_URL https://shipstarter.domain.dev.atlassian.io

COPY . /opt/service
WORKDIR /opt/service
RUN npm rebuild

EXPOSE 8080

CMD npm start
