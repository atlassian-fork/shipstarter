'use strict';


import * as projectsApi from './api/projects';
import * as taskAPI from './api/tasks';
import HomePage from './pages/home';
import Project from './entities/project';
import ProjectPage from './pages/project';
import * as jiraApi from './api/jira'
import skate from 'skatejs';
import _ from 'lodash';

const IS_OFFLINE = !window.AP;
const IS_LOADING_BY_DEFAULT = true;

export default skate('shipstarter-router', {
    events: {
        'projectDeleted': function(e) {
            this.goToHome();
        }
    },
    created() {
        const shipstarterRouter = this;
        window.AP.getLocation(location => {
            window.AP.getUser((user) => {
                window.AP.require(['history'], (history) => {
                    jiraApi.callGet(`/rest/api/2/user?key=${user.key}`).then((jiraUser) => {
                        shipstarterRouter.history = history;
                        shipstarterRouter.currentUser = jiraUser;
                        shipstarterRouter.startRouter(history, location);
                    });
                });
            });
        });

    },
    properties: {
        isLoading: {
            attr: true,
            init: IS_LOADING_BY_DEFAULT,
            type: Boolean
        }
    },
    template() {
        this.innerHTML = `<div id="shipstarter-page"></div>`;
    },
    prototype: {
        get page() {
            return this.querySelector('#shipstarter-page');
        },

        getProjects() {
            const shipstarterRouter = this;

            return new Promise((resolve, reject) => {
                projectsApi.getProjectsByKey()
                    .then((projects = []) => {
                        if (!Array.isArray(projects)) {
                            projects = [];
                        }
                        var toResolve = {};
                        var total = 1;
                        resolve(projects);
                        // for each project, go get tasks
                        // _.each(projects, (project, key) => {
                        //   toResolve[project.id] = project;
                        //   taskAPI.getTasksByProject(project.id).then((data) => {
                        //     toResolve[project.id].tasks = data;
                        //     if(total === projects.length){
                        //       resolve(projects);
                        //       this.isLoading = false;
                        //     }
                        //     total++;
                        //   });

                        // });
                    })
                    .catch(() => {
                        shipstarterRouter.isLoading = false;
                        reject();
                    });
            });

        },
        goToHome() {
            const shipstarterRouter = this;
            if (IS_OFFLINE) {
                return shipstarterRouter.replacePage(new HomePage());
            }
            shipstarterRouter.history.pushState('');
            shipstarterRouter.getProjects()
                .then((projects) => {
                    shipstarterRouter.replacePage(new HomePage({
                        projects: projects,
                        currentUser: shipstarterRouter.currentUser
                    }))
                });
        },
        goToProject(key) {
            const shipstarterRouter = this;
            shipstarterRouter.isLoading = true;
            if(key) {
                projectsApi.getProjectsByKey(key)
                    .then((projects = []) => {
                        const desiredProject = projects[0];
                        taskAPI.getTasksByProject(desiredProject.get('key')).then((tasks) => {
                            let projectProperties = desiredProject.data;
                            projectProperties.currentUser = shipstarterRouter.currentUser;
                            projectProperties.tasks = tasks;
                            const projectPage = new ProjectPage(projectProperties);
                            shipstarterRouter.replacePage(projectPage);
                        });
                    });
            } else {
                shipstarterRouter.replacePage(new ProjectPage({currentUser: shipstarterRouter.currentUser}));
            }

        },
        startRouter(history, hostUrl) {
            const shipstarterRouter = this;
            const initialRoute = hostUrl.split('#')[1];

            const router = window.Router({
                '/': shipstarterRouter.goToHome.bind(shipstarterRouter),
                '/!': shipstarterRouter.goToHome.bind(shipstarterRouter),
                '/!project': shipstarterRouter.goToProject.bind(shipstarterRouter),
                '/project': shipstarterRouter.goToProject.bind(shipstarterRouter),
                '/project/:id': shipstarterRouter.goToProject.bind(shipstarterRouter),
                '/!project/:id': shipstarterRouter.goToProject.bind(shipstarterRouter)
            });

            router
                .configure({
                    before() {
                        if (history) {
                            history.pushState(router.getRoute().join('/'));
                        }
                    }
                })
                .init(initialRoute || '/');
        },
        replacePage(element) {
            const shipstarterRouter = this;
            shipstarterRouter.isLoading = false;
            const currentPage = shipstarterRouter.page;
            if (!currentPage.childNodes.length) {
                return currentPage.appendChild(element);
            }
            currentPage.replaceChild(element, currentPage.firstChild);
        }
    }
});
