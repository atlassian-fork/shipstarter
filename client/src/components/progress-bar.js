'use strict';

import skate from 'skatejs';

export default skate('shipstarter-progress-bar', {
  properties: {
    percent: {
      attr: true,
      init: 0,
      type: Number,
      set (value) {
        window.AJS.progressBars.update(this.querySelector('.aui-progress-indicator'), value / 100);
      }
    }
  },
  template: function () {
    this.innerHTML = `
      <div class="aui-progress-indicator">
        <span class="aui-progress-indicator-value"></span>
      </div>
    `;
  }
});
