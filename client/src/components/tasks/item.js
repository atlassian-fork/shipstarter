'use strict';

import skate from 'skatejs';
import * as projectsApi from '../../api/projects';
import * as taskApi from '../../api/tasks';
import taskEntity from '../../entities/task';
import * as viewHelpers from '../../utils/view-helpers';
import _ from 'lodash';

// Check whether the current user has pledged for this task
function hasPledgedToTask(el) {
    return el.pledgees ? _.findIndex(el.pledgees, pledgee => pledgee.key === el.currentUser.key) >= 0 : false;
}

function createPledgeeAvatar (avatarUrl, fullName) {
    return `
    <span class="aui-avatar aui-avatar-medium aui-avatar-project shipstarter-pledgee-avatar" title="${fullName}">
        <span class="aui-avatar-inner">
            <img src="${avatarUrl}" alt="${fullName}" title="${fullName}">
        </span>
    </span>`;
}

function taskFromTaskItem(taskElement) {
    const thisTask = taskElement;
    return new taskEntity({
        id: thisTask.id,
        key: thisTask.key,
        title: thisTask.title,
        description: thisTask.description,
        projectId: thisTask.projectId,
        issuetype: thisTask.issuetype
    });
}

function pledge(taskElement, user) {
    return taskApi.pledgeUserToTask(user, taskElement.id);
}

function unPledge(taskElement, user) {
    let newPledgees = taskElement.pledgees.slice();
    _.remove(newPledgees, (pledgee) => {
        return pledgee.key === user.key;
    });

    taskElement.pledgees = newPledgees;

    return taskApi.updatePledgeesForTask(taskElement.id, taskElement.pledgees);
}

export default skate('shipstarter-tasks-item', {
    events: {
        'click': function(e) {
            const thisTask = this;
            if(thisTask.canPledge && !hasPledgedToTask(thisTask)) {
                pledge(thisTask, thisTask.currentUser).then(newPledgees => {
                    thisTask.pledgees = newPledgees
                });
            } else  if(hasPledgedToTask(thisTask)) {
                unPledge(thisTask, thisTask.currentUser);
            }
        },
        'save': function (e) {
            const thisTask = this;
            const updatedOrNewTask = taskFromTaskItem(thisTask);
            updatedOrNewTask.set(e.detail.name, e.detail.value);
            if (updatedOrNewTask.data.id) {
                taskApi.updateTask(updatedOrNewTask.data.id, updatedOrNewTask);
            } else {
                taskApi.createTask(thisTask.projectId, updatedOrNewTask).then(function (newIssue) {
                    const parsedNewIssue = newIssue;
                    thisTask.id = parsedNewIssue.id;
                    thisTask.key = parsedNewIssue.key;
                });
            }
            thisTask.title = updatedOrNewTask.get("title");
            thisTask.description = updatedOrNewTask.get("description");

            return false; //prevent propagation so that the project page doesn't detect a change
        }
    },
    extends: 'li',
    properties: {
        canPledge: {
            attr: true
        },
        description: {
            set(value) {
                this.querySelector('.shipstarter-tasks-item-desc shipstarter-inline-edit').textContent = value;
            }
        },
        title: {
            set(value) {
                this.querySelector('.shipstarter-tasks-item-title shipstarter-inline-edit').textContent = value;
            }
        },
        pledgees: {
            set(value) {
                this.hasPledged = hasPledgedToTask(this);
                let avatarMarkup = '';
                value.forEach((user) => {
                    avatarMarkup += createPledgeeAvatar(user.avatarUrls['32x32'], user.displayName);
                });
                this.querySelector('.shipstarter-task-pledgees').innerHTML = avatarMarkup;
            }
        },
        editable: {
            set(value) {
                [].slice.apply(this.querySelectorAll('shipstarter-inline-edit')).forEach((node) => {
                    node.editable = value;
                });
            }
        },
        hasPledged: {
            attr: true
        },
        currentUser: {}
    },
    template: function () {
        var title = this.querySelector('item-title');
        var description = this.querySelector('description');
        this.innerHTML = `
            <span class="shipstarter-tasks-item-title">
                <shipstarter-inline-edit name="title">${title ? title.textContent : ''}</shipstarter-inline-edit>
            </span>
            <p class="shipstarter-tasks-item-desc">
                <shipstarter-inline-edit name="description">${description ? description.textContent : ''}</shipstarter-inline-edit>
            </p>
            <div class="shipstarter-task-pledgees"></div>
        `;

    }
});