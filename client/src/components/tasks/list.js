'use strict';

import arrayOf from '../../utils/property-types/array-of';
import skate from 'skatejs';
import taskItem from './item';
import taskEntity from '../../entities/task';
import * as taskApi from '../../api/tasks';

function emptyTask(element) {
    return new taskEntity({
        description: 'Enter description',
        title: 'Enter title',
        projectId: element.projectId
    });
}

function addTaskToList(element, newTask) {
    element.tasks = element.tasks.concat([newTask]);
}

export default skate('shipstarter-tasks-list', {
    properties: {
        projectId: {
            type: String
        },
        editable: {
            attr: true,
            init: true,
            type: Boolean
        },
        tasks: {
            init() {
                return [];
            },
            type: arrayOf(taskEntity),
            set(tasks) {
                var items = this.querySelector('ol');
                items.innerHTML = '';
                tasks.forEach(taskData => {
                    taskData.data.currentUser = this.currentUser;
                    taskData.data.editable = this.editable;
                    taskData.data.canPledge = !this.editable;
                    const newTaskItem = new taskItem(taskData.data)
                    items.appendChild(newTaskItem);
                    taskApi.getTaskPledgees(newTaskItem.id).then(pledgees => {
                        newTaskItem.pledgees = pledgees;
                    });
                });
            }
        },
        currentUser: {}
    },
    template() {
        this.innerHTML = `
      <ol>
        <li>There are no tasks.</li>
      </ol>
      <button id="shipstarter-tasks-list-add" class="aui-button">
        <span class="aui-icon aui-icon-small aui-iconfont-add">Add</span>
        Add task
      </button>
    `;
    },
    events: {
        'click #shipstarter-tasks-list-add': function () {
            addTaskToList(this, emptyTask(this));
            this.querySelector('ol > li:last-child shipstarter-inline-edit').editing = true;
        }
    }
});
