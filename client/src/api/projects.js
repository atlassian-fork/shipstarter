import Project from '../entities/project';
import * as jira from './jira';
import _ from 'lodash';

const PROJECT_KEY_REGEX = /^S(\d){9}$/;
const PROJECT_URL = `/rest/api/2/project`;
const PROJECT_SEARCH_URL = '/rest/api/2/search';
export const METADATA_ISSUE_SUMMARY = '___DO_NOT_DELETE___SHIPSTARTER PROJECT META DATA';

//API Functions ------------------------------------------------------------------------------------

function projectsByTopicJQL(topics) {
    return `labels in (${topics.join(',')}) AND summary ~ "${METADATA_ISSUE_SUMMARY}"`;
}

function convertToProject(project) {
    return new Project(project);
}

function extractProject(issue) {
    return issue.fields.project;
}

function byProjectId(issue) {
    return issue.fields.project.id;
}

function getShipstarterProjects(projects) {
    return _.filter(projects, project => {
        return project.key.match(PROJECT_KEY_REGEX);
    });
}

//----- Project search methods --------
export function getProjectsByTopics(topics) {

    return jira.callGet(`${PROJECT_SEARCH_URL}?jql=${projectsByTopicJQL(topics)}`).then(data => {
        const metaDataIssuesFound = data.issues
        const uniqueProjects = _.uniq(metaDataIssuesFound, byProjectId).map(extractProject);

        return _.map(getShipstarterProjects(uniqueProjects), convertToProject);
    });
}

function projectUrl(projectKeyOrId) {
    return `${PROJECT_URL}/${projectKeyOrId || ''}`;
}


export function getProjectsByKey(key, startAt = 0, maxResults = 9999999) {
    let url = projectUrl(key);

    url += `?expand=description&startAt=${startAt}&maxResults=${maxResults}`;

    return jira.callGet(url).then((data) => {
        if (!Array.isArray(data)) {
            data = [data];
        }

        var combined = getShipstarterProjects(data);

        let projects = combined.map(data => new Project(data));

        return projects;
    });
}

export function getProjectEntityByKey(project_key, entity_key) {
    var url = `${PROJECT_URL}/${project_key}/properties/${entity_key}`;
    return new Promise((resolve, reject) => {
        jira.callGet(url).then(function (data) {
            resolve({project_key: project_key, value: data.value});
        }, jira.handleErrors(reject));
    });
}

function projectKey(issue) {
    return issue.fields.project.key;
}

export function getProjectsByPledgees(pledgees) {
    return jira.jqlSearch(`pledgees IN (${pledgees.join(",")})`)
        .then(function(rawJiraData){
            return _.chain(rawJiraData.issues)
                .uniq(projectKey)
                .map('fields.project')
                .value();
        });
}

//----- Project update methods --------

export function saveProject(project) {
    var project_url = PROJECT_URL;
    var projectProperties = JSON.stringify(project.getProjectProperties());

        var jiraPromise;
        if (project.data.id) {
            jiraPromise = jira.callUpdate(project_url + '/' + project.data.key, projectProperties, 'PUT');
        } else {
            jiraPromise = jira.callUpdate(project_url, projectProperties, 'POST');
        }

        return jiraPromise.then((data) => {
            var proj = new Project(_.merge(data, project.data));
            return proj;
        });
}

export function deleteProject(projectKeyOrId) {
    return jira.callDelete(`${PROJECT_URL}/${projectKeyOrId}`);
}

function updateMetaIssueKey(projectKeyOrId, metaIssueId) {
    return jira.callUpdate(projectMetaIssueUrl(projectKeyOrId), JSON.stringify({metaIssueId: metaIssueId})).then(() => {
        return metaIssueId;
    });
}

//----- Project metadata methods --------


function projectMetaIssueUrl(projectKeyOrId) {
    return `${projectUrl(projectKeyOrId)}/properties/metaIssue`;
}

function extractTopics(metaIssue) {
    return metaIssue.fields.labels;
}

export function getProjectTopics(projectKeyOrId) {
    return getMetaIssueKey(projectKeyOrId)
        .then(jira.getIssue)
        .then(extractTopics);
}

export function createMetaIssue(projectKeyOrId) {
    var data = {
        fields: {
            summary: METADATA_ISSUE_SUMMARY,
            description: 'shipstarter uses this issue to make projects searchable, please don\'t delete it'
        }
    }

    return jira.callGet(`${PROJECT_URL}/${projectKeyOrId}`).then((project) => {
        const parsedProject = project;
        const desiredIssueTypeId = _.find(parsedProject.issueTypes, (issueType) => {return issueType.name === 'Task'}).id;
        data.fields.issuetype = {
            id: desiredIssueTypeId
        };
        return jira.createIssue(parsedProject.id, data);
    }).then((data) => {
        return updateMetaIssueKey(projectKeyOrId, data.id);
    })
}

export function getMetaIssueKey(projectKeyOrId) {
    return jira.callGet(projectMetaIssueUrl(projectKeyOrId)).then((data) => {
        const metaIssueId = data.value.metaIssueId;
        if(metaIssueId) {
            return metaIssueId;
        }
        //account for when the value is undefined but it exists
        return createMetaIssue(projectKeyOrId);
    }).catch((error) => {
        if(error.status === 404) {
            return createMetaIssue(projectKeyOrId);
        }
    });
}
