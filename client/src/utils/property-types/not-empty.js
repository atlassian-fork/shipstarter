'use strict';

export default function (defaultValue, defaultType = String) {
  return function (value) {
    return defaultType(value || defaultValue);
  };
}
