'use strict';

import * as projectsApi from '../api/projects';
import * as taskApi from '../api/tasks';
import * as topicsApi from '../api/topics';
import ProjectEntity from '../entities/project';
import * as notifications from '../utils/notifications';
import * as viewHelpers from '../utils/view-helpers'
import propertyArray from '../utils/property-types/array-of';
import propertyNotEmpty from '../utils/property-types/not-empty';
import skate from 'skatejs';
import TaskList from '../components/tasks/list';
import taskItem from '../components/tasks/item';
import inlineEdit from '../components/inline-edit';
import inlineEditSelect from '../components/inline-edit-select';

export default skate('shipstarter-project-page', {
    events: {
        'save': function (e) {
            var projectPage = this;
            projectPage.status = 'loading';
            var project = new ProjectEntity();

            project.set('description', projectPage.description);
            project.set('name', projectPage.name);

            if (projectPage.id) {
                project.set('id', projectPage.id);
            }

            if (projectPage.key) {
                project.set('key', projectPage.key);
            }

            project.set('lead', projectPage.currentUser.key);
            project.set('projectTypeKey', 'business');     //TODO change to software when I can enable software for local instance
            var saveProjectPromise = projectsApi.saveProject(project);
            saveProjectPromise.then(project => {
                var isUpdating = projectPage.id;  //if projectId is set on the page it means we're updating an existing project
                viewHelpers.updateViewModelWithData(this, project.data);
                if (!isUpdating) {
                    notifications.createProjectSuccess(project.data);
                }
                projectPage.status = `Saved ${new moment().format('h:mmA')}`;
            }).catch(notifications.saveProjectError);
        },
        'save #topics-selector': function (e) {
            const projectPage = this;
            topicsApi.updateTopics(projectPage.id, e.detail.value);
            projectPage.status = `Saved ${new moment().format('h:mmA')}`;
        },
        'click #delete-button': function(e) {
            const projectPage = this;
            AJS.dialog2('#delete-project-dialog').show();
        }
    },
    properties: {
        status: {
          set(value) {
              const projectStatus = this.querySelector('.project-status');
              if(value === "loading") {
                  projectStatus.textContent = 'Updating...';
              } else {
                  projectStatus.textContent = value;
              }
          }
        },
        project: {
            set(value) {
                console.log('project passed in:', value);
            }
        },
        description: {
            init: '',
            type: propertyNotEmpty('Your description goes here.'),
            get() {
                return this.querySelector('.shipstarter-project-description').textContent;
            },
            set(value) {
                this.querySelector('.shipstarter-project-description').textContent = value;
            }
        },
        id: {
            set(value) {
                projectsApi.getProjectTopics(value).then((topics) => {
                    const topicsSelector = this.querySelector('#topics-selector');
                    skate.init(topicsSelector);
                    topicsSelector.value = topics;
                });
            }
        },
        issueTypes: {
            type: propertyArray()
        },
        key: {},
        tasks: {
            set(value) {
                this.list.innerHTML = '';
                const newTaskList = new TaskList({
                    projectId: this.id
                });
                newTaskList.editable = this.isCurrentUserLead();
                newTaskList.currentUser = this.currentUser;
                newTaskList.tasks = value;
                this.list.appendChild(newTaskList);
            }
        },
        name: {
            init: '',
            type: propertyNotEmpty('Your title goes here'),
            get() {
                return this.querySelector('.shipstarter-project-title').textContent;
            },
            set(value) {
                this.querySelector('.shipstarter-project-title').textContent = value;
                this.querySelector('.shipstarter-project-breadcrumb').textContent = value;
            }
        },
        currentUser: {
            set (value) {
                const inlineEdits = this.querySelectorAll('shipstarter-inline-edit');
                [].forEach.call(inlineEdits, (inlineEdit) => {
                    inlineEdit.editable = this.isCurrentUserLead();
                });
                this.querySelector('#topics-selector').editable = this.isCurrentUserLead();
            }
        }
    },
    prototype: {
        get list() {
            return this.querySelector('.shipstarter-tasks-list-container');
        },
        isCurrentUserLead() {
            const projectPage = this;
            return projectPage.lead ? projectPage.currentUser.key === projectPage.lead.key : true;
        }
    },

    detached() {
        if(document.querySelector('#delete-project-dialog')) {
            AJS.dialog2('#delete-project-dialog').remove();
        }
    },
    template() {
        this.innerHTML = `
            <shipstarter-app>
                <section role="dialog" id="delete-project-dialog" class="aui-layer aui-dialog2 aui-dialog2-small aui-dialog2-warning" aria-hidden="true">
                    <header class="aui-dialog2-header">
                        <h2 class="aui-dialog2-header-main">Delete project</h2>
                        <a class="aui-dialog2-header-close">
                            <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                        </a>
                    </header>
                    <div class="aui-dialog2-content">
                        <p>Are you sure you want to delete this project?</p>
                    </div>
                    <footer class="aui-dialog2-footer">
                        <div class="aui-dialog2-footer-actions">
                            <button id="confirm-delete" class="aui-button aui-button-primary">Delete</button>
                            <button id="dialog-close-button" class="aui-button aui-button-link">Cancel</button>
                        </div>
                    </footer>
                </section>
                <shipstarter-banner class="banner-project">
                    <div class="project-actions">
                        <small class="project-action project-status">Loaded</small>
                        <span id="delete-button" class="project-action aui-icon aui-icon-small aui-iconfont-delete"></span>
                    </div>
                    <h2><shipstarter-inline-edit class="shipstarter-project-title" name="name"></shipstarter-inline-edit></h2>
                </shipstarter-banner>
                <shipstarter-content>
                    <div class="aui-group">
                    <div class="aui-item shipstarter-breadcrumbs">
                        <a href="#/">ShipStarter</a><span> / <span class="shipstarter-project-breadcrumb"></span></span>
                    </div>
                    </div>
                    <div class="aui-group">
                        <div class="aui-item ss-project-overview">
                            <h2>Overview</h2>
                            <shipstarter-inline-edit type="textarea" class="shipstarter-project-description" name="description"></shipstarter-inline-edit>
                        </div>
                        <div class="aui-item ss-project-tasks">
                            <h2>Tasks</h2>
                            <div class="shipstarter-tasks-list-container">
                            </div>
                        </div>
                    </div>
                    <div class="aui-group">
                        <shipstarter-inline-edit-select id="topics-selector"></shipstarter-inline-edit-select>
                    </div>
                </shipstarter-content>
            </shipstarter-app>
        `;

        const topicsSelector = this.querySelector("#topics-selector");
        topicsSelector.addEventListener('start-editing', (e) => {
            topicsApi.getAvailableTopics().then((topics) => {
                e.detail.element.renderSelect(topics);
            });
        });

        topicsSelector.emptyState = '<small> No topics have been picked yet </small>';

        this.querySelector('#confirm-delete').addEventListener('click', () => {
            const projectPage = this;
            AJS.dialog2('#delete-project-dialog').hide();
            AJS.dialog2('#delete-project-dialog').remove();
            projectsApi.deleteProject(projectPage.id).then(data => {
                skate.emit(projectPage, "projectDeleted");
                notifications.deleteProjectSuccess({name: projectPage.name});
            });
        });

        this.querySelector('#dialog-close-button').addEventListener('click', () => {
            AJS.dialog2('#delete-project-dialog').hide();
        });
    }
});